import datetime
import requests
import json
import numpy as np
import pandas as pd

# details Mehul's personal access token for the oura API
with open('credentials.json', 'r') as f:
    cred = json.load(f)

# specifies the start and end parameter names for the api call
with open('api_params.json', 'r') as f:
    api_params_dict = json.load(f)

# API Header providing access token
headers = {
    'Authorization': 'Bearer ' + cred['personal_access_token']
}

# API base base_uri
base_uri = 'https://api.ouraring.com/v2/usercollection/'

# turns datetime type to string formatted to work in API call
def datetime_to_string(date : datetime.date) -> str:
    return date.isoformat() + 'T00:00:00-08:00'

# turns API string format to datetime type
def string_to_datetime(dt : str) -> datetime.date:
    return datetime.date.fromisoformat(dt[0:10])
    
# finds difference between two ISO date strings
def datetime_diff(start : str, end : str) -> int:
    start = string_to_datetime(start)
    end = string_to_datetime(end)
    diff = end - start
    return diff.days

def format_api_date(date:str, api:str) -> str:
    if api == 'heartrate':
        return date
    else:
        return date[0:10]

# parent function to extract heart rate data from API
def get_oura_data(start:str, end:str, api:str, base_uri:str=base_uri, 
    headers:dict=headers) -> pd.DataFrame:
    params = {
        'start': start, 
        'end': end 
    }
    return get_data_recursively(params=params, base_uri=base_uri, api=api, 
        headers=headers)


# local recursive function to call API on any size date range
def get_data_recursively(params:dict, 
    base_uri:str, api:str, headers:dict, 
    api_params_dict:dict=api_params_dict) -> pd.DataFrame:

    start = params['start']
    end = params['end']

    MAX_DAYS = 30

    timerange = datetime_diff(start,end)

    # base case where date range is small enough for API call. Note params 
    # altered here since heartrate API params have different name and format 
    # than others.
    if timerange <= MAX_DAYS:
        params = {
            api_params_dict[api]['first param'] : format_api_date(start,api),
            api_params_dict[api]['second param'] : format_api_date(end,api)
        }
        response = requests.request('GET', base_uri+api, headers=headers,
            params=params, verify=False)

        try:
            return pd.DataFrame(response.json()['data']).reset_index()
        except:
            print(f'Error - {response.text}')
    
    # recursive step, call API on first 30 days, append to remaining data.
    else:
        mid = datetime_to_string(string_to_datetime(start) +
            datetime.timedelta(days=MAX_DAYS))

        params1 = {
            'start' : start,
            'end' : mid
        }
        params2 = {
            'start' : mid,
            'end' : end
        }

        return pd.concat(
            [
                get_data_recursively(params1, base_uri, api, headers),
                get_data_recursively(params2, base_uri, api, headers)
            ]
        )
